package com.consultation;

import com.consultation.entity.Account;
import com.consultation.repository.AccountRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@SpringBootTest
@AutoConfigureMockMvc
public class AccountControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private AccountRepository accountRepository;



    @Test
    @Transactional
    public void registration() throws Exception {
        mockMvc.perform(post("/register")
                        .param("email", "test@test")
                        .param("firstName", "test")
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                        .characterEncoding("utf-8")
                )
                .andDo(print());

        Account account = accountRepository.findAccountByEmail("test@test");

        Assertions.assertNotNull(account);
        Assertions.assertEquals(account.getFirstName(), "test");

    }

}
