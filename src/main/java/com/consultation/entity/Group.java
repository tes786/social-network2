package com.consultation.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "\"group\"")
public class Group {
    @Id
    private long id;
    private String name;

    @ManyToMany
    @JoinTable(
            name = "account_group",
            joinColumns = @JoinColumn(name = "group_id"),
            inverseJoinColumns = @JoinColumn(name = "account_id")
    )
    private List<Account> members;
}
