package com.consultation;

import com.consultation.entity.Account;
import com.consultation.entity.Group;
import com.consultation.entity.Post;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.transaction.Transactional;
import java.util.List;

@Component
@Transactional
public class ContactRepository {

    @Autowired
    public EntityManager em;

    public List<Account> getAllAccounts() {
        return em
                .createQuery("select c from Account c", Account.class)
                .getResultList();
    }

    public Account getOneAccount(long id) {
        return em.find(Account.class, id);
    }

    public Post getOnePost(long id) {
        return em.find(Post.class, id);
    }

    public List<Post> getAllPosts() {
        return em.createQuery("select c from Post c", Post.class)
                .getResultList();
    }

    public List<Group> getGroups() {
        return em.createQuery("select g from Group g", Group.class)
                .getResultList();
    }

    public void addPost(Post post) {
        em.persist(post);
    }

    public void deletePost(long id) {
        em.remove(em.find(Post.class, id));
    }

    public void updatePost(Post post) {
        em.merge(post);
    }
}
