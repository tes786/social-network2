package com.consultation.view;

import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
public class AccountView {
    private long id;
    private String firstName;
    private String lastName;
    private LocalDate dateOfBirth;
    private String city;
    private List<PostView> posts;
}
