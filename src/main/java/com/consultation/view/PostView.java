package com.consultation.view;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class PostView {
    private long id;
    private String text;
    private LocalDateTime publicationTime;
}
