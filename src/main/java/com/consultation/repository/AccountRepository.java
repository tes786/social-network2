package com.consultation.repository;

import com.consultation.entity.Account;
import org.springframework.data.repository.CrudRepository;

public interface AccountRepository extends CrudRepository<Account, Long> {
    Account findAccountByEmail(String email);
}
