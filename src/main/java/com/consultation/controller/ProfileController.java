package com.consultation.controller;

import com.consultation.ContactService;
import com.consultation.LoggedAccount;
import com.consultation.view.AccountView;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequiredArgsConstructor
public class ProfileController {

    private final ContactService contactService;
    private final LoggedAccount loggedAccount;

    @GetMapping("/profile")
    public String profile(Model model) {
        long id = loggedAccount.getLoggedAccount().getId();
        AccountView accountView = contactService.getAccount(id);
        model.addAttribute("account", accountView);
        return "profile";
    }

    @PostMapping("/profile/new-post")
    public String addPost(@ModelAttribute("text") String text, Model model) {
        long accountId = loggedAccount.getLoggedAccount().getId();
        contactService.addPost(text, accountId);
        return profile(model);
    }

    @PostMapping("/profile/delete-post/{postId}")
    public String deletePost(@PathVariable Long postId,
                             Model model) {
        contactService.deletePost(postId);
        return profile(model);
    }

    @PostMapping("/profile/edit-post/{postId}")
    public String editPost(@PathVariable Long postId,
                           @ModelAttribute("text") String text,
                           Model model) {
        contactService.updatePost(postId, text);
        return profile(model);
    }

}
