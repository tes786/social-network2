package com.consultation.controller;

import com.consultation.ContactService;
import com.consultation.view.LoginForm;
import com.consultation.view.RegistrationForm;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
@RequiredArgsConstructor
public class AccountController {

    private final ContactService service;

    @GetMapping("/registration")
    public String registration() {
        return "registration";
    }

    @PostMapping("/register")
    public String registerUser(@Validated @Valid RegistrationForm form) {
        service.registerAccount(form);
        return "redirect:/";
    }

    @GetMapping("/login")
    public String login() {
        return "login";
    }

}
