package com.consultation.controller;

import com.consultation.ContactService;
import com.consultation.entity.Account;
import com.consultation.entity.Group;
import com.consultation.entity.Post;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ExampleController {

    @Autowired
    private ContactService contactService;

    @GetMapping("/accounts")
    public List<Account> accounts() {
        List<Account> allAccounts = contactService.getAllAccounts();
        return allAccounts;
    }

    @GetMapping("/posts")
    public List<Post> posts() {
        return contactService.getAllPosts();
    }

    @GetMapping("/groups")
    public List<Group> groups() {
        return contactService.getAllGroups();
    }

    @GetMapping("/printname")
    public void printName(@RequestParam String name) {
        System.out.println(name);
    }
}
