package com.consultation;

import com.consultation.entity.Account;
import com.consultation.repository.AccountRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class DatabaseUserDetailsService implements UserDetailsService {

    private final AccountRepository accountRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Account account = accountRepository.findAccountByEmail(username);
        UserDetails user =
                User.withDefaultPasswordEncoder()
                        .username(account.getEmail())
                        .password(account.getPassword())
                        .roles(account.getRole().name())
                        .build();
        return user;
    }
}
