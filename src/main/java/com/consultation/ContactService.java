package com.consultation;

import com.consultation.entity.Account;
import com.consultation.entity.Group;
import com.consultation.entity.Post;
import com.consultation.repository.AccountRepository;
import com.consultation.view.AccountView;
import com.consultation.view.LoginForm;
import com.consultation.view.PostView;
import com.consultation.view.RegistrationForm;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Component
@RequiredArgsConstructor
public class ContactService {

    private final ContactRepository contactRepository;
    private final AccountRepository accountRepository;
    private final LoggedAccount loggedAccount;

    public List<Account> getAllAccounts() {
        return contactRepository.getAllAccounts();
    }

    public AccountView getAccount(long id) {
        Account a = contactRepository.getOneAccount(id);

        AccountView av = new AccountView();
        av.setId(a.getId());
        av.setFirstName(a.getFirstName());
        av.setLastName(a.getLastName());
        av.setDateOfBirth(a.getDateOfBirth());
        av.setCity(a.getCity());

        List<PostView> posts = new ArrayList<>();
        for (Post p : a.getPosts()) {
            PostView pw = new PostView();
            pw.setId(p.getId());
            pw.setText(p.getText());
            pw.setPublicationTime(p.getPublicationTime());
            posts.add(pw);
        }

        av.setPosts(posts);

        return av;
    }

    public void registerAccount(RegistrationForm form) {
        Account account = new Account();
        account.setFirstName(form.getFirstName());
        account.setLastName(form.getLastName());
        account.setEmail(form.getEmail());
        account.setDateOfBirth(form.getDateOfBirth());
        account.setCity(form.getCity());
        account.setPassword(form.getPassword());
        account.setRole(Account.Role.USER);

        accountRepository.save(account);
    }

    public List<Post> getAllPosts() {
        return contactRepository.getAllPosts();
    }

    public List<Group> getAllGroups() {
        return contactRepository.getGroups();
    }

    public void addPost(String text, long accountId) {
        Account account = contactRepository.getOneAccount(accountId);
        Post p = new Post();
        p.setText(text);
        p.setAccount(account);
        p.setPublicationTime(LocalDateTime.now());
        contactRepository.addPost(p);
    }

    public void deletePost(long postId) {
        contactRepository.deletePost(postId);
    }

    public void updatePost(long postId, String text) {
        Post post = contactRepository.getOnePost(postId);
        post.setText(text);
        contactRepository.updatePost(post);
    }

}
